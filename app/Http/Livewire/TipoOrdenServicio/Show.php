<?php

namespace App\Http\Livewire\TipoOrdenServicio;

use App\Models\User;
use Livewire\Component;
use App\Models\TipoServicio;
use Livewire\WithPagination;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;


class Show extends Component
{
	use WithPagination;

	public $view = 'create';

	/**
	 * Propiedad del modelo
	 * @var [type]
	 */
	public $descripcion;

	/**
	 * Id
	 * @var [type]
	 */
	public $tipo_servicio_id =0;

	public $search = "";

	protected $queryString = ['search'];


    protected function rules()
	{
	    return [
	        'descripcion' => [
	        	'required',
	        	'min:4',
	        	Rule::unique('tipo_orden_servicio', 'descripcion')->ignore($this->tipo_servicio_id),
	        ],
	    ];
	}

	/**
	 * Vista a renderizar
	 * @return [type] [description]
	 */
    public function render()
    {
        return view('livewire.tipo-orden-servicio.show',[
        	'tipos' => TipoServicio::where('descripcion', 'ILIKE',"%{$this->search}%")
        		->orderBy('id','desc')
        		->paginate(10),
        ])
        ->extends('layouts.dashboard.dashboard');
    }

    /**
     * Funcion para guardar el tipo de servicio
     * @return [type] [description]
     */
    public function guardar()
    {
    	$this->validate();

    	$tipo = TipoServicio::create([
    		'user_id' => Auth::user()->id,
    		'descripcion' => $this->descripcion,
    	]);

    	$this->editar($tipo->id);
    }

    /**
     * Funcion para editar el tipo de servicio
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editar($id)
    {
    	$tipo = TipoServicio::find($id);
    	$this->tipo_servicio_id = $tipo->id;
    	$this->descripcion = $tipo->descripcion;
    	$this->view = 'edit';
    }

    public function update()
    {
    	$this->validate();

    	$tipo = TipoServicio::find($this->tipo_servicio_id);
    	$tipo->update([
    		'user_id' => Auth::user()->id,
    		'descripcion' => $this->descripcion,
    	]);

    	$this->default();
    }

    /**
     * Funcion para eliminar un tipo de servicio
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
    	TipoServicio::destroy($id);
    }

    /**
     * Comportamiento por defecto
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function default()
    {
    	$this->descripcion = '';
    	$this->view = 'create';
    }

    public function updated($field)
    {
        $this->validate();
    }
}
