<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\User;
use App\Models\Client;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('home', [
            'countUsers' => User::all()->count(),
            'countClients'  => Client::all()->count(),
            'countCities'  => City::all()->count(),
        ]);
    }
}
