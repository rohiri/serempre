<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('cities.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("cities.create", [
            'ruta' => ['id' => 'cities','route' => 'cities.store'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            City::create([
                'cod' => $request->cod,
                'name' => $request->name
            ]);
            DB::commit();

            return redirect('cities')->with('status', 'Ciudad Guardado Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return View::make("cities.create", [
            'ruta' => [
                'id' => 'cities',
                'method' => 'PUT',
                'route'  => ['cities.update', $city->id], 'autocomplete' => 'on'],
                'city'   => $city,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        DB::beginTransaction();

        try {
            City::where('id', $city->id)
                ->update([
                    'cod' => $request->cod,
                    'name'  => $request->name,
            ]);

            DB::commit();

            return redirect('cities')->with('status', 'Ciudad Editada Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            City::find($id)->delete();

            DB::commit();

            return Response::json([
                'url' => env('APP_URL') . '/cities',
                'message' => 'Ciudad Eliminada Correctamente',
            ]);
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function ajaxCities(Request $request)
    {
        return Datatables::of(City::all())
            ->addColumn('actions', function ($city) {
                return '
                <a href="' . route('cities.edit', $city->id) . '" class="btn btn-warning fa fa-edit"></a>
                <a href="#" class="btn btn-success fa fa-eye"></a>
                <a href="#" class="btn btn-danger fa fa-trash" id="delete" data-url="/cities/' . $city->id . '"></a>
                ';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
