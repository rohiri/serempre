<?php

namespace App\Http\Controllers;

use App\Models\City;
use Ramsey\Uuid\Uuid;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("client.create", [
            'ruta' => ['id' => 'client','route' => 'clients.store', 'files' => true],
            'cities' => City::pluck('name', 'id')->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        $uuid = Uuid::uuid4()->toString();

        if ($request->hasFile('photo_profile')) {
            $nombreArchivo = $uuid . '.' . $request->file('photo_profile')->getClientOriginalExtension();//nombre del archivo en el servidor
            $path = $request->file('photo_profile')->storeAs('/images', $nombreArchivo, 'public');
        }

        try {
            // dd([
            //     'path' => $path,
            //     'name' => $nombreArchivo
            // ]);
            Client::create([
                'cod' => $request->cod,
                'name' => $request->name,
                'city' => $request->city,
                'profile_photo' => $path
            ]);
            DB::commit();

            return redirect('clients')->with('status', 'Cliente Guardado Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('clients')->with('status', 'No se pudo guardar  Correctamente!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return View::make("client.create", [
            'ruta' => [
                'id' => 'clients',
                'method' => 'PUT',
                'route'  => ['clients.update', $client->id], 'autocomplete' => 'on'],
                'client' => $client,
                'cities'   => City::pluck('name', 'id')->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        DB::beginTransaction();

        try {
            Client::where('id', $client->id)
                ->update([
                    'cod' => $request->cod,
                    'name' => $request->name,
                    'city' => $request->city,
            ]);

            DB::commit();

            return redirect('clients')->with('status', 'Cliente Editado Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            Client::find($id)->delete();

            DB::commit();

            return Response::json([
                'url' => env('APP_URL') . '/clients',
                'message' => 'Cliente Eliminado Correctamente',
            ]);
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function ajaxClient(Request $request)
    {
        return Datatables::of(Client::with('City')->get())
            ->addColumn('actions', function ($client) {
                return '
                <a href="' . route('clients.edit', $client->id) . '" class="btn btn-warning fa fa-edit"></a>
                <a href="#" class="btn btn-success fa fa-eye"></a>
                <a href="#" class="btn btn-danger fa fa-trash" id="delete" data-url="/clients/' . $client->id . '"></a>
                ';
            })
            ->addColumn('url_photo', function ($client) {
                return '
                <a href="#" class="btn btn-default fa fa-print" id="view" data-url="/storage/' . $client->profile_photo . '"></a>
                ';
            })
            ->rawColumns(['actions', 'url_photo'])
            ->make(true);
    }
}
