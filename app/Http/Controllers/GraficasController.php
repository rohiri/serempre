<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class GraficasController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function recaudoMensual()
    {

        $recaudos = DB::SELECT("
        	SELECT date_part('year', r.fecha_recibo::date) as ano, date_part('month', r.fecha_recibo::date) as mes, sum(r.valor::DOUBLE PRECISION) as total_mes
            FROM recibo_caja r
            WHERE r.fecha_recibo BETWEEN :fechaInicio AND :fechaFin
            GROUP BY date_part('year', r.fecha_recibo::date), date_part('month', r.fecha_recibo::date)
            ORDER BY 1, 2
        	",[
        		'fechaInicio' => '2021-01-01',
        		'fechaFin' => '2021-12-31'
        	]);

        //return Response::json($recaudos);

        $meses = [
        	'Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ];
        $valores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    	foreach ($recaudos as $mes) {
    		$valores[$mes->mes - 1] = $mes->total_mes;
    	}

        return Response::json([
	        	'texto' => $meses,
	        	'informacion' => $valores
       	]);
    }

    public function recaudoDiarioMes(Request $request)
    {
    	$recaudos = DB::SELECT("
        	SELECT date_part('year', r.fecha_recibo::date) as ano, date_part('month', r.fecha_recibo::date) as mes, date_part('day', r.fecha_recibo::date) as dia, sum(r.valor::DOUBLE PRECISION) as total_dias
                FROM recibo_caja r
                WHERE r.fecha_recibo BETWEEN :fechaInicio AND :fechaFin
                AND date_part('month', r.fecha_recibo::date) = :mes
                GROUP BY date_part('year', r.fecha_recibo::date), date_part('month', r.fecha_recibo::date), date_part('day', r.fecha_recibo::date)
                ORDER BY 1, 2, 3
        	",[
        		'fechaInicio' => '2021-01-01',
        		'fechaFin' => '2021-12-31',
        		'mes' => $request->mes ?? 1,
        ]);

    	$dias = ['Día 1', 'Día 2', 'Día 3', 'Día 4', 'Día 5', 'Día 6', 'Día 7', 'Día 8', 'Día 9', 'Día 10', 'Día 11', 'Día 12', 'Día 13', 'Día 14', 'Día 15', 'Día 16', 'Día 17', 'Día 18', 'Día 19', 'Día 20', 'Día 21', 'Día 22', 'Día 23', 'Día 24', 'Día 25', 'Día 26', 'Día 27', 'Día 28', 'Día 29', 'Día 30', 'Día 31'];
        $valores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    	foreach ($recaudos as $dia) {
    		$valores[$dia->dia - 1] = $dia->total_dias;
    	}

    	return Response::json([
	        	'texto' => $dias,
	        	'informacion' => $valores
       	]);
    }
}
