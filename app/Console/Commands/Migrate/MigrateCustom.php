<?php

namespace App\Console\Commands\Migrate;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigrateCustom extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'william:migrate {--stub=} {--class=} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'William Custom Migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $stub = $this->option('stub');
        $class = $this->option('class');
        $table = $this->option('table');

        // Comprobamos que existe nuestro stub
        $file = app_path() . "/Console/Commands/Migrate/stubs/" . $stub . ".stub";
        if (!$this->files->isFile($file)) {
            $this->error("There is no stub with this name.");
            exit;
        }

        // Obetenemos el archivo stub
        $contents = $this->files->get($file);

        // Creamos Nuestra clase de acuerdo a nuestra variable
        $contents = str_replace('{{class}}', studly_case($class), $contents);

        // Comprobamos si el comando tiene una variable table, si no se sale
        if (strstr($contents, '{{table}}') and empty($table)) {
            $this->error("A table name should be given by --table option.");
            exit;
        }
        $contents = str_replace('{{table}}', $table, $contents);

        // Creamos nuestra migracion de acuerdo a nuestro stub
        $this->files->put(base_path() . "/database/migrations/" . date("Y_m_d_His") . "_" . $class . ".php", $contents);
    }
}
