<?php

namespace App\Console\Commands\Migrate;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class WillianRunMigrations extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'william:run {--database= :The Database connection to use}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Migrations and seeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Para Confirmar si estamos en produccion
        if (! $this->confirmToProceed()) {
            return 1;
        }

        // Llamamos al comando migrate:refresh
        $this->call('migrate:refresh', array_filter([
            '--database' => $this->option('database'),
        ]));

        //Ejecutamos Seeders
        $this->call('db:seed', ['--force' => true]);

        return 0;
    }
}
