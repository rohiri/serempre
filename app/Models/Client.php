<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Tabla del modelo
     *
     * @var string
     */
    protected $table = 'client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod', 'name', 'city', 'profile_photo',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Mutador de descripcion
     * @param [type] $value [description]
     */
    public function setCodeAttribute($value)
    {
        $this->attributes['cod'] = ucwords(strtolower($value));
    }

    /**
     * Mutador de descripcion
     * @param [type] $value [description]
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city');
    }
}
