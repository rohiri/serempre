<div class="box-body">
    @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
    {{-- @include('partials.errors') --}}
    <div class="row">
        <div class="col-md-12">
            <h4 class=""><i class="fa fa-book"></i> Informacion Inicial</h4>
        </div>
        <div class="col-md-4">
            {!! Field::text('cod', $client->cod ?? null,[
                'label' => 'Código de Cliente',
                'class' => '',
                'ph' => '000001',
                'required'
            ])!!}
        </div>
        <div class="col-md-4">
            {!! Field::text('name', $client->name ?? null,[
                'label' => 'Nombre del Cliente',
                'class' => '',
                'ph' => 'Boston',
                'required'
            ])!!}
        </div>
        <div class="col-md-4">
            {!! Field::select('city', $cities  ?? [],
                $client->city ?? null,
                ['label'=>'Ciudad','class'=> 'select2', 'style'=>'width: 100%', 'required'])
            !!}
        </div>
        <div class="col-md-4">
            {!! Field::file('photo_profile',[
                'label' => 'Imagen Producto',
                'class' => '',
            ])!!}
        </div>
    </div>
</div>
<div class="overlay loading" style="display:none;">
    <i class="fa fa-refresh fa-spin"></i>
</div>
