@extends('layouts.dashboard.dashboard')
@section('content')
    <section class="content-header">
      <h1>
        Dashboard
        <small>Panel de Control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
    <div class="visible-print text-center">
    {!! QrCode::size(100)->generate('https://www.youtube.com/watch?v=lkAUS2UxgmE'); !!}
      <p>Scan me to return to the original page.</p>
  </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{$countUsers}}</h3>
              <p>Usuarios Del Sistema</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-plus"></i>
            </div>
            <a href="{!! URL('user') !!}" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$countClients}}</h3>
              <p>Clientes Del Sistema</p>
            </div>
            <div class="icon">
              <i class="fa fa-home"></i>
            </div>
            <a href="{!! URL('client') !!}" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$countCities}}</h3>
              <p>Ciudades del Sistema</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar-o"></i>
            </div>
            <a href="{!! URL('cities') !!}" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="row">

      </div>
  </section>


@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {

});
</script>
@endsection