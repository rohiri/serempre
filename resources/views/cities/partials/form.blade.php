<div class="box-body">
            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif
            {{-- @include('partials.errors') --}}
            <div class="row">
                <div class="col-md-12">
                    <h4 class=""><i class="fa fa-book"></i> Informacion Inicial</h4>
                </div>
                <div class="col-md-2">
                    {!! Field::text('cod', $city->cod ?? null,[
                        'label' => 'Código de Ciudad',
                        'class' => '',
                        'ph' => '000001',
                        'required'
                    ])!!}
                </div>
                <div class="col-md-2">
                    {!! Field::text('name', $city->name ?? null,[
                        'label' => 'Nombre de la Ciudad',
                        'class' => '',
                        'ph' => 'Boston',
                        'required'
                    ])!!}
                </div>
            </div>
        </div>
        <div class="overlay loading" style="display:none;">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
