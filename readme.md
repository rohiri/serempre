# Prueba de desarrollo.

_Prueba de desarrollo para Serempre._

### Paquetes Terceros

- AdminLTE

### Pre-requisitos

- Php 7.3.0 con phpCli habilitado para la ejecución de comando.
- Postgresql > 9.6
- Composer
- Extensión pdo_pgsql habilitada.
- Node & npm

### Instalación

1. Clonar el repositorio en la carperta del servidor web.

```sh
git clone https://gitlab.com/rohiri/serempre.git
```

2. Instalar paquetes.

```sh
composer install
```
3. Copiar archivo  `.env.example` a  `.env`

```sh
`cp .env.example .env`
```

4. Configure las variables de entorno para base de datos
- `DB_HOST=` Variable de entorno para el host de BD.
- `DB_PORT=` Variable de entorno para el puerto de BD.
- `DB_DATABASE=` Variable de entorno para el nombre de BD.
- `DB_USERNAME=` Variable de entorno para el usuario de BD.
- `DB_PASSWORD=` Variable de entorno para la contraseña de BD.

5. Configure las Variables para envio de correo (Sugerencia Mailtrap)
- `MAIL_DRIVER=smtp`
- `MAIL_HOST=smtp.mailtrap.io`
- `MAIL_PORT=2525`
- `MAIL_USERNAME=`
- `MAIL_PASSWORD=`
- `MAIL_ENCRYPTION=null`

6. En la raíz del sitio ejecutar.
- `php artisan key:generate` Genera la llave para el cifrado del proyecto.
- `composer install` Instala dependencias de PHP
- `npm install` Instala dependencias de javascript
- `npm run dev` Genera la llave para el cifrado del proyecto.
- `php artisan migrate:refresh --seed` Ejecuta migraciones y seeders

8. Usuarios Predefinidos.

Email|Password
 ------ | ------
wiltorc2430@gmail.com|root


9. Para Ejecutar migraciones Personalizadas
```sh
php artisan william:migrate --stub=blank --class=CreateFireTable --table=fire
```

10. Para Ejecutar Migraciones y Seeders (El parametro database es la conexion a base de datos a usar)
```sh
php artisan william:run --database=pgsql
```
11. Para visualizar las Fotos de Perfil (Nuevo Feature), Ejecutar el comando:
```sh
php artisan storage:link
```

## Autor

**William Ricardo Torres Curtidor**
