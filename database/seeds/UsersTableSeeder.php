<?php

use Carbon\Carbon;
use App\Models\User;
use App\Models\Marca;
use App\Models\Barrio;
use App\Models\Bodega;
use App\Models\Tercero;
use App\Models\Producto;
use App\Models\TipoPredio;
use App\Models\CentroCosto;
use App\Models\TipoPersona;
use App\Models\TipoTercero;
use App\Models\TipoServicio;
use App\Models\TerceroCampos;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'william',
            'email' => 'wiltorc2430@gmail.com',
            'password' => bcrypt('root'),
        ]);
    }
}
